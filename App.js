/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import HomeScreen from './src/pages/Home';
import SettingsStack from './src/components/SettingsStack';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Octicon from 'react-native-vector-icons/Octicons';

function App() {
  const Tab = createBottomTabNavigator();
  return (
    <NavigationContainer>
      <Tab.Navigator >
        <Tab.Screen 
          name="Home" 
          component={HomeScreen} 
          options={{
            tabBarIcon:({color,size})=>(
              <Octicon name="home" color={color} size={size}/>
            ),
          }}
        />
        <Tab.Screen 
          name="Settings" 
          component={SettingsStack} 
          options={{
            tabBarIcon:({color,size})=>(
              <Octicon name="gear" color={color} size={size}/>
            ),
          }}
        />
      </Tab.Navigator>
    </NavigationContainer>
  );
}

export default App;
