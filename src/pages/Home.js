import React from 'react'
import { TouchableOpacity, StyleSheet, Text, View, SafeAreaView, FlatList } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';

const Data=[
  {
    id:'001',
    title:'Fruit',
  },
  {
    id:'002',
    title:'Vegetable',
  },
  {
    id:'003',
    title:'Dairy',
  },
]

function Home ({navigation}){
  const renderItem = ({item}) => (
    <TouchableOpacity style={styles.item} onPress={() => navigation.navigate('Detail',{
      itemId:item.id,
      itemTitle:item.title,
    })}>    
      <Text style={styles.title}>{item.title}</Text>
    </TouchableOpacity>
  )
  return(
    <SafeAreaView style={styles.container}>
      <FlatList
        data={Data}
        renderItem={renderItem}
        keyExtractor={item=>item.id}
      />
    </SafeAreaView>
  )
}

function Detail({route}){
  console.log(route)
  const { itemId, itemTitle } = route.params;
  return(
    <View style={styles.det}>
      <Text>itemTitle:{itemTitle}</Text>
      <Text>itemId:{itemId}</Text>
    </View>
  )
}

function HomeScreen() {
  const Stack = createStackNavigator();
  return (
    <Stack.Navigator screenOptions={{headerTitleAlign: 'center', headerTintColor:'white', headerStyle: { backgroundColor: '#53B175' }}}>
      <Stack.Screen name="Home" component={Home}/>
      <Stack.Screen name="Detail" component={Detail} />
    </Stack.Navigator>

  );
}

const styles = StyleSheet.create({
  container:{
    flex: 1, 
    backgroundColor:'#ed9121',
  },
  item:{
    padding:20,
    marginVertical:8,
    marginHorizontal:16,
    borderRadius:10,
    backgroundColor:'lightblue',
  },
  title:{
    fontSize:20,
    color:'white',
  },
  det:{
    flex: 1, 
    alignItems: 'center', 
    justifyContent: 'center',
  }
});

export default HomeScreen;
