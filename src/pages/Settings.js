import React from 'react'
import { StyleSheet, Text, View } from 'react-native';

function Settings() {
  return (
    <View style={styles.container}>
      <Text style={styles.txt}>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis, quod velit! Magni aliquid ipsam, 
        excepturi dolores inventore iusto quis temporibus officiis maxime nisi fuga doloribus quam! Aliquam atque 
        amet reprehenderit!
      </Text>
    </View>
  )
}

const styles = StyleSheet.create({
  container:{
    flex: 1, 
    alignItems: 'center', 
    justifyContent: 'center',
  },
  txt:{
    textAlign:'justify',
    padding:20,
  }
});

export default Settings
