import React from 'react';
import Settings from '../pages/Settings';
import { createStackNavigator } from '@react-navigation/stack';

export default function SettingsStack(){
  const SettingsStack = createStackNavigator();
  return(
    <SettingsStack.Navigator screenOptions={{headerTitleAlign: 'center', headerTintColor:'white', headerStyle: { backgroundColor: '#5383EC' }}}>
      <SettingsStack.Screen name="Settings" component={Settings}/>
    </SettingsStack.Navigator>
  );
}

